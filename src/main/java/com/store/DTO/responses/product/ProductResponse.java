package com.store.DTO.responses.product;

import com.store.DTO.requests.product.ProductRequest;

public class ProductResponse {
    public Boolean error;
    public ProductRequest product;
}
