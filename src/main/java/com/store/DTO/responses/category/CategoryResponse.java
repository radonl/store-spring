package com.store.DTO.responses.category;

import com.store.DTO.requests.category.CategoryRequest;

public class CategoryResponse {
    public Boolean error;
    public CategoryRequest category;
}
