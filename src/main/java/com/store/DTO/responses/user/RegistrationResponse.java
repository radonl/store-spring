package com.store.DTO.responses.user;

public class RegistrationResponse {
    public Long userId;
    public Boolean error;
}
