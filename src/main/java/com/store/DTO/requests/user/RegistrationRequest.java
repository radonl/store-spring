package com.store.DTO.requests.user;

public class RegistrationRequest {
    public String firstName;
    public String lastName;
    public String email;
    public String phone;
    public String address;
    public String password;
}
