package com.store.DTO.requests.product;

public class ProductRequest {
    public Long id;
    public String name;
    public String description;
    public Integer price;
    public Long categoryId;
}
