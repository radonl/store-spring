package com.store.services.imp;

import com.store.DTO.requests.category.CategoryRequest;
import com.store.DTO.responses.category.CategoryResponse;
import com.store.models.Category;
import com.store.repositories.CategoryRepository;
import com.store.services.interfaces.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CategoryService implements ICategoryService {
    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public CategoryResponse createCategory(CategoryRequest request) {
        CategoryResponse response = new CategoryResponse();
        try {
            Category category = new Category();
            category.setDescription(request.getDescription());
            category.setName(request.getName());
            request.setId(this.categoryRepository.save(category).getId());
            response.error = false;
            response.category = request;
            return response;
        } catch (Exception e) {
            response.error = true;
            return response;
        }
    }

    @Override
    public CategoryResponse updateCategory(CategoryRequest request) {
        CategoryResponse response = new CategoryResponse();
        Optional<Category> category = this.categoryRepository.findById(request.getId());
        if(category.isPresent()){
            Category model = category.get();
            model.setName(request.getName());
            model.setDescription(request.getDescription());
            this.categoryRepository.save(model);
            response.category = request;
        } else {
            response.error = true;
        }
        return response;
    }

    @Override
    public Iterable<Category> getCategories() {
        return categoryRepository.findAll();
    }

    @Override
    public Boolean removeCategory(Long id) {
        try {
            Optional<Category> category = this.categoryRepository.findById(id);
            category.ifPresent(this.categoryRepository::delete);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
