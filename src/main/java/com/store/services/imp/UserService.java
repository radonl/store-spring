package com.store.services.imp;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.store.DTO.requests.user.AuthorizationRequest;
import com.store.DTO.requests.user.RegistrationRequest;
import com.store.DTO.responses.user.RegistrationResponse;
import com.store.models.Role;
import com.store.models.User;
import com.store.repositories.RoleRepository;
import com.store.repositories.UserRepository;
import com.store.services.interfaces.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

@Service
public class UserService implements IUserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    private static final BCryptPasswordEncoder bCrypt = new BCryptPasswordEncoder();

    @Override
    public RegistrationResponse registration(RegistrationRequest registration) {
        try {
            Optional<Role> clientRole = this.roleRepository.findByName("Client");
            User user = new User();
            user.setFirstName(registration.firstName);
            user.setLastName(registration.lastName);
            user.setEmail(registration.email);
            user.setAddress(registration.address);
            user.setPhone(registration.phone);
            user.setPassword(bCrypt.encode(registration.password));
            clientRole.ifPresent(user::setRole);
            RegistrationResponse response = new RegistrationResponse();
            response.error = false;
            response.userId = userRepository.save(user).getId();
            return response;
        } catch (Exception exception) {
            RegistrationResponse response = new RegistrationResponse();
            response.error = true;
            return response;
        }

    }

    @Override
    public String authorization(AuthorizationRequest request) {
        Optional<User> user = this.userRepository.findByEmail(request.email);
        if (user.isPresent()) {
            User u = user.get();
            if (bCrypt.matches(request.password, u.getPassword())) {
                Algorithm algorithm = Algorithm.HMAC256("secret");
                Date dt = new Date();
                Calendar c = Calendar.getInstance();
                c.setTime(dt);
                c.add(Calendar.DATE, 30);
                dt = c.getTime();
                return JWT.create()
                        .withIssuer("auth0")
                        .withExpiresAt(dt)
                        .sign(algorithm);
            }
        }
        return "";
    }


}
