package com.store.services.imp;

import com.store.DTO.requests.product.ProductRequest;
import com.store.DTO.responses.product.ProductResponse;
import com.store.models.Category;
import com.store.models.Product;
import com.store.repositories.CategoryRepository;
import com.store.repositories.ProductRepository;
import com.store.services.interfaces.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.Optional;

@Service
public class ProductService implements IProductService {

    private CategoryRepository categoryRepository;

    private ProductRepository productRepository;

    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Autowired
    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public ProductResponse createProduct(ProductRequest request) {
        Optional<Category> category = this.categoryRepository.findById(request.categoryId);
        ProductResponse response = new ProductResponse();
        if(category.isPresent()) {
            Product product = new Product();
            product.setDescription(request.description);
            product.setName(request.name);
            product.setPrice(request.price);
            product.setCategory(category.get());
            request.id = this.productRepository.save(product).getId();
            response.product = request;
        } else {
            response.error = true;
        }
        return response;
    }

    @Override
    public ProductResponse updateProduct(ProductRequest request) {
        ProductResponse response = new ProductResponse();
        try {
            Optional<Product> product = this.productRepository.findById(request.id);
            Optional<Category> category = this.categoryRepository.findById(request.categoryId);
            if(product.isPresent() && category.isPresent()) {
                Product p = product.get();
                p.setDescription(request.description);
                p.setName(request.name);
                p.setPrice(request.price);
                p.setCategory(category.get());
                this.productRepository.save(p);
                response.error = false;
                response.product = request;
            } else {
                response.error = true;
            }
        } catch (Exception e) {
            response.error = true;
        }
        return response;
    }

    @Override
    public Iterable<Product> getProducts() {
        return this.productRepository.findAll();
    }

    @Override
    public Boolean removeProduct(Long id) {
        try {
            Optional<Product> category = this.productRepository.findById(id);
            category.ifPresent(this.productRepository::delete);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
