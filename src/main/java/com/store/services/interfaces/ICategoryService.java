package com.store.services.interfaces;

import com.store.DTO.requests.category.CategoryRequest;
import com.store.DTO.responses.category.CategoryResponse;
import com.store.models.Category;

public interface ICategoryService {
    public CategoryResponse createCategory(CategoryRequest request);
    public CategoryResponse updateCategory(CategoryRequest request);
    public Iterable<Category> getCategories();
    public Boolean removeCategory(Long id);
}
