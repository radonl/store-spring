package com.store.services.interfaces;

import com.store.DTO.requests.user.AuthorizationRequest;
import com.store.DTO.requests.user.RegistrationRequest;
import com.store.DTO.responses.user.RegistrationResponse;

public interface IUserService {
    public abstract RegistrationResponse registration(RegistrationRequest request);
    public abstract String authorization(AuthorizationRequest request);
}
