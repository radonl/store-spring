package com.store.services.interfaces;

import com.store.DTO.requests.product.ProductRequest;
import com.store.DTO.responses.product.ProductResponse;
import com.store.models.Product;

public interface IProductService {
    public ProductResponse createProduct(ProductRequest request);
    public ProductResponse updateProduct(ProductRequest request);
    public Iterable<Product> getProducts();
    public Boolean removeProduct(Long id);
}
