package com.store.controllers;


import com.store.DTO.requests.user.AuthorizationRequest;
import com.store.DTO.requests.user.RegistrationRequest;
import com.store.DTO.responses.user.RegistrationResponse;
import com.store.services.imp.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path="/users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping(path = "/registration")
    RegistrationResponse registration(@RequestBody RegistrationRequest request) {
        return userService.registration(request);
    }

    @PostMapping(path = "/authorization")
    ResponseEntity<String> authorization(@RequestBody AuthorizationRequest request) {
        String token = userService.authorization(request);
        if(token.isEmpty()) {
            return new ResponseEntity<String>("Authorization failed!", HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<String>(token, HttpStatus.OK);
    }
}