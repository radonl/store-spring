package com.store.controllers;

import com.store.DTO.requests.category.CategoryRequest;
import com.store.DTO.responses.category.CategoryResponse;
import com.store.models.Category;
import com.store.services.imp.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path="/categories")
public class CategoriesController {
    @Autowired
    private CategoryService categoryService;

    @PostMapping(path = "/")
    CategoryResponse create(@RequestBody CategoryRequest request) {
        return this.categoryService.createCategory(request);
    }

    @PutMapping(path = "/")
    CategoryResponse update(@RequestBody CategoryRequest request) {
        return this.categoryService.updateCategory(request);
    }

    @GetMapping(path = "/")
    Iterable<Category> get() {
        return this.categoryService.getCategories();
    }

    @DeleteMapping(path = "/{id}")
    Boolean delete(@PathVariable Long id) {
        return this.categoryService.removeCategory(id);
    }
}
