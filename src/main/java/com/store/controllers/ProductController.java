package com.store.controllers;

import com.store.DTO.requests.product.ProductRequest;
import com.store.DTO.responses.product.ProductResponse;
import com.store.models.Product;
import com.store.services.imp.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path="/products")
public class ProductController {
    @Autowired
    private ProductService productService;

    @PostMapping(path = "/")
    ProductResponse create(@RequestBody ProductRequest request) {
        return this.productService.createProduct(request);
    }

    @PutMapping(path = "/")
    ProductResponse update(@RequestBody ProductRequest request) {
        return this.productService.updateProduct(request);
    }

    @GetMapping(path = "/")
    Iterable<Product> get() {
        return this.productService.getProducts();
    }

    @DeleteMapping(path = "/{id}")
    Boolean delete(@PathVariable Long id) {
        return this.productService.removeProduct(id);
    }
}
